extends CanvasLayer

var nextScenePath

var preview_node
var above_node
var level_node
var actual_node
var actual_stage
var actual_points
var best_points

func _ready():
	get_tree().change_scene("res://Scenes/Intro/IntroScreen.tscn")
	get_node("MusicIntro").play()

func fade_to(path):
	nextScenePath = path
	get_node("Anim").play("Fade")

func fade_black_to(path, stage):
	preview_node = actual_node
	nextScenePath = path
	actual_stage = stage
	get_node("Anim").play("FadeBlack")
	
func change_scene():
	if nextScenePath != null:
		get_tree().change_scene(nextScenePath)
		for c in preview_node.get_children():
			c.free()
		preview_node.free()

func put_above(path):
	if above_node != null:
		return
	get_tree().paused = true
	above_node = load(path).instance()
	get_tree().get_root().add_child(above_node)

	
func clear_above():
	if above_node == null:
		return	
	get_tree().paused = false
	get_tree().get_root().remove_child(above_node)
	above_node = null
	

func stop_music():
	get_node("MusicIntro").stop()
	get_node("MusicGnome").stop()
	get_node("MusicKDE").stop()

func play_music(music):
	get_node(music).play()

func free_child_level_node():
	if(level_node != null):
		for c in level_node.get_children():
			c.free()

func free_level_node():
	pass


