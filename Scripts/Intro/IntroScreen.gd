extends Node


var control
var itemMenuAtual
var audioChange
var score

func _ready():
	control = get_node("Control")
	control.get_child(0).grab_focus()
	audioChange = get_node("AudioChange")
	score = get_node("Score")
	generate_user_files()
	get_tree().set_auto_accept_quit(false)
	Transition.actual_node = get_tree().get_current_scene()

func _input(event):
	if(Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down")):
		audioChange.play()

func _on_PlayButton_pressed():
	get_node("AudioSelect").play()
	Transition.fade_black_to("res://Scenes/Intro/StageScreen.tscn", null)

func _on_ExitButton_pressed():
	Transition.queue_free()
	queue_free()
	get_tree().quit()


func _on_Control_gui_input(ev):
	pass


func _on_ScoreButton_pressed():
	get_node("AudioSelect").play()
	self.populate_score()
	get_node("Score/BackButton").grab_focus()
	control.hide()
	score.show()

func populate_score():
	var gnome_points = get_node("Score/GnomePoints")
	var kde_points = get_node("Score/KdePoints")
	
	gnome_points.set_text(str(self.read_best_points_gnome()) + " points")
	kde_points.set_text(str(self.read_best_points_kde()) + " points")


func write_best_points(p):
	var file = File.new()
	file.open("res://Scenes/" + Transition.actual_stage + ".txt", file.WRITE)
	file.store_string(str(p))
	file.close()


func generate_user_files():
	var file = File.new()
	if !file.file_exists("user://gnome.txt"):
		file.open("user://gnome.txt", file.WRITE)
		file.store_string("0")
		file.close()
	
	
	var file2 = File.new()
	if  !file2.file_exists("user://kde.txt"):
		file2.open("user://kde.txt", file2.WRITE)
		file2.store_string("0")
		file2.close()



func read_best_points_gnome():
	var file = File.new()
	file.open("user://gnome.txt", file.READ)
	var text = file.get_as_text()
	var points = text.split("\n")
	file.close()
	return points[0]

func read_best_points_kde():
	var file = File.new()
	file.open("user://kde.txt", file.READ)
	var text = file.get_as_text()
	var points = text.split("\n")
	file.close()
	return points[0]


func _on_BackButton_pressed():
	get_node("AudioSelect").play()
	control.get_child(0).grab_focus()
	score.hide()
	control.show()
