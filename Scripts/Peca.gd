extends Node2D

var mascote_preloader

var slot1
var slot2

var posy
var pos_anterior_y = 0

var bottom = false
var block = false

var xRand
var x
var y

var novoMascote
var novoMascote2

var is_signal_emmited = false

var velocidade

onready var level

# trata-se da matrix no momento de criacao da peça
var matrix

signal limite(x,y,t1,t2,r)
signal removed

func _ready():
	level = get_node("../../")
	gera_2_slots(2)
	xRand = int(rand_range(1, 5))
	set_pos_matrix(xRand,0)
	set_process_input(true)
	set_process(true)
	matrix = get_node("../").matrix
	mascote_preloader = weakref(load("res://Scenes/Game/Mascote.tscn"))


func _process(delta):

	# vertical movement

	if(!block):
		var posy = get_position().y + velocidade * delta;
		set_position(Vector2(get_position().x, posy))
		self.y = get_pos_matrix_y(posy)
		
		if(is_collided()):
			block = true

	if(block):
		# emmit signal
		if(!is_signal_emmited):
			get_node("AudioCollided").play()
			emit_signal("limite", x, y, slot1, slot2)
			is_signal_emmited = true


	#  Verify
	## if is not bottom
	## if is not colided with other block
	## if is not colided with wall or box



func _exit_tree():
	#level.free()
	
	if(!novoMascote):
		pass
		#print("mascote 1 liberado")
	else:
		#print("Liberando mascote 1")
		novoMascote.free()
	
	if(!novoMascote2):
		pass
		#print("mascote 2 liberado")
	else:
		#print("Liberando mascote 2")
		novoMascote2.free()
	
	queue_free()


func gera_2_slots_init():
	slot1 = get_node("Slot1")
	slot2 = get_node("Slot2")

	slot1.gera_mascote_1()
	slot2.gera_mascote_2(slot1.mascote.tipo_mascote)
	
	slot1.mascote.connect("death", self, "_on_death_remove_slot1")
	slot2.mascote.connect("death", self, "_on_death_remove_slot2")


func gera_2_slots(n):
	var mascote_preview_1
	var mascote_preview_2
	
	mascote_preview_1 = get_mascote_preview_1()
	mascote_preview_2 = get_mascote_preview_2()
	
	slot1 = get_node("Slot1")
	slot2 = get_node("Slot2")

	slot1.gera_mascote_1(n)
	slot2.gera_mascote_2(slot1.mascote.tipo_mascote, n)

	slot1.update_mascote(mascote_preview_1.tipo_mascote, n)
	slot2.update_mascote(mascote_preview_2.tipo_mascote, n)
	


	slot1.mascote.connect("death", self, "_on_death_remove_slot1")
	slot2.mascote.connect("death", self, "_on_death_remove_slot2")





func get_mascote_preview_1():
	return level.get_mascote_1()
	
func get_mascote_preview_2():
	return level.get_mascote_2()



func gen_2_mascotes():
	novoMascote = mascote_preloader.get_ref().instance();
	novoMascote.num_pos = 1

	novoMascote2 = mascote_preloader.get_ref().instance();
	novoMascote2.num_pos = 2

	var s1 = get_node("Slot1")
	var s2 = get_node("Slot2")



	while(novoMascote.tipo_mascote == novoMascote2.tipo_mascote):
		novoMascote2.free()
		novoMascote2 = mascote_preloader.get_ref().instance();
		novoMascote2.num_pos = 2

	s1.add_child(novoMascote)
	s2.add_child(novoMascote2)


#vertical collision

# return true if next vertical position is wall (null) or other block (1,2,3,4, ... )
# return false if next vertical position is empty space (0)

func is_collided():
	
	# get slot2 and verify r (rotation)
	var s1 = get_node("Slot1")
	var s2 = get_node("Slot2")
	
	var lr = s2.r
	var iscollided = false
	
	
	if(y < 6):
		
		
		# see anotations
		if(lr == 1):
			if(matrix[x][y+1] != null or matrix[x+1][y+1] != null):
				iscollided = true
	
		if(lr == 2):
			
			if(y+2 <= 7):
				if(matrix[x][y+2] != null):
					iscollided = true
			else:
				iscollided = true
	
		if(lr == 3):
			if(matrix[x][y+1] != null or matrix[x-1][y+1] != null):
				iscollided = true
	
		if(lr == 4):	
			if(matrix[x][y+1] != null):
				iscollided = true

	else:
		iscollided = true
	
	return iscollided



#horizontal movement and rotation

# verify before run command
# return true if next horizontal position is null (wall)
# return false if next horizontal position is empty space (0)

func _input(event):

	if(!block):
		var s2 = get_node("Slot2")
		var lr = s2.r
		var move = false
		var lx = int(round(x))
		var ly = int(round(y+0.5))

		
		if(Input.is_action_pressed("ui_left")):
			
			# if if is not a wall or other block in the left side then move
			# x = 0,1,2, ..., 5



			
			if(lr == 1):
				if( (lx >= 1) and (matrix[lx-1][ly] == null) and (matrix[lx-1][ly-1] == null) ):
					move = true
			if(lr == 2):
				if( (lx >= 1) and (matrix[lx-1][ly] == null) and (matrix[lx-1][ly+1] == null ) ):
					move = true
			if(lr == 3):
				if( (lx >= 2) and (matrix[lx-2][ly] == null) and (matrix[lx-2][ly-1] == null) ):
					move = true
			if(lr == 4):
				if( (lx >= 1) and (matrix[lx-1][ly] == null) ):
					move = true

			if(move):
				left()


		if(Input.is_action_pressed("ui_right")):
			
			# return true if next horizontal position is null (wall)
			# return false if next horizontal position is empty space (0)
			
			if(lr == 1):
				if( (lx <= 3) and (matrix[lx+2][ly] == null) and (matrix[lx+2][ly-1] == null) ):
					move = true
			
			if(lr == 2):
				if( (lx <= 4) and (matrix[lx+1][ly] == null) and (matrix[lx+1][ly+1] == null) ):
					move = true
					
			if(lr == 3):
				if( (lx <= 4) and (matrix[lx+1][ly] == null) and (matrix[lx+1][ly-1] == null) ):
					move = true
			
			if(lr == 4):
				if( (lx <= 4) and (matrix[lx+1][ly] == null) and (matrix[lx+1][ly-1] == null) ):
					move = true
					
			if(move):
				right()
			
				
		if(Input.is_action_pressed("ui_down")):

			if(!is_collided()):

				down()

		if(Input.is_action_pressed("ui_up")):
			# vertical
			move = true

			if(ly < 6):

				if(lr == 1 and matrix[lx][ly+1] != null):
					move = false
				
				if(lr == 2 and x == 0):
					move = false
				
				if(lr == 2 and (matrix[lx-1][ly+1] != null or matrix[lx-1][ly] != null)):
					move = false
				
				if((lr == 4 and x == 5)):
					move = false
				
				if(lr == 4 and lx <= 4 and matrix[lx+1][ly+1] != null):
					move = false
				
			else:
				move = false	
			
			if(move):
				rotate()




func left():
	get_node("AudioGirando").play()
	set_pos_matrix_x(x-1)


func right():
	get_node("AudioGirando").play()
	
	if(x < 5):
		set_pos_matrix_x(x+1)

func down():
	set_pos_matrix(x,y+1)
		
func rotate():
	get_node("AudioGirando").play()
	# see the drawing - realocate slots
	
	var s2 = get_node("Slot2")
	s2.rotate()


func remove_slot(num):
	#print("remove_slot(",num,")")
	
	if num == 1:
		
		var slot1 = get_node("Slot1");
		
		for s1 in slot1.get_children():
			s1.queue_free()
		
		slot1.queue_free()
	
	elif num == 2:
		var slot2 = get_node("Slot2");
		
		for s2 in slot2.get_children():
			s2.queue_free()
		
		slot2.queue_free()


func move_slot_para_baixo(num, local_pos_y):
	var slot
	
	if num == 1:
		slot = get_node("Slot1");
		
	elif num == 2:
		slot = get_node("Slot2");

	slot.move_vertical_to(local_pos_y)

func move_para(to_y):
	set_pos_matrix_y(to_y)


func set_pos_matrix(x,y):
	var lx = int(x)
	var ly = int(y)
	
	self.x = x
	self.y = y
	

	set_position(
		Vector2( 
			311 + (lx * 104),
			(ly * 104) + (52 + 26)
		)
	)

func set_pos_matrix_x(x):
	var lx = int(x)
	
	self.x = x
	
	set_position(
		Vector2(
			311 + (lx * 104),
			 get_position().y
		)
		
	)


func set_pos_matrix_y(y):
	var ly = int(y)
	
	self.y = y
	
	set_position(
		Vector2(
			 get_position().x,
			(ly * 104) + (52 + 26)
		)
		
	)


func get_pos_matrix(x, y):

	var retx = (x - 311)/104
	var rety = (y - (52 + 26 ))/104
	
	return [retx, rety]

	
func get_pos_matrix_x(x):
	var retx = (x - 311)/104
	return retx

func get_pos_matrix_y(y):
	var rety = (y - (52 + 26))/104
	return rety


func remove_slot1():
	var slot1 = get_node("Slot1")
	var mascote = slot1.get_child(1)
	mascote.set_anim_death()
	
	yield( mascote.get_node("Sprite") , "finished" )
	#mascote.set_anim_death_yield()

	if(!has_slot2()):
		return true
	else:
		return false

func death_slot1():
	var slot1 = get_node("Slot1")
	var mascote = slot1.get_child(1)
	mascote.set_anim_death()

	slot1.free();

	if(!has_slot2()):
		return true
	else:
		return false


func remove_slot2():
	var slot2 = get_node("Slot2")
	var mascote = slot2.get_child(1)
	
	mascote.set_anim_death()
	
	yield( mascote.get_node("Sprite") , "finished" )
	
	slot2.free();
		
	if(!has_slot1()):
		return true
	else:
		return false
	

func death_slot2():
	var slot2 = get_node("Slot2")
	var mascote = slot2.get_child(1)
	mascote.set_anim_death()

	slot2.free();
		
	if(!has_slot1()):
		return true
	else:
		return false


func has_slot1():
	var slot1 = get_node("Slot1")
	
	if(slot1 != null):
		return true
	else:
		return false


func has_slot2():
	var slot2 = get_node("Slot2")
	
	if(slot2 != null):
		return true
	else:
		return false

func get_pontos_slot1():
	var slot1 = get_node("Slot1")
	var mascote = slot1.get_child(1)
	
	return mascote.pontos_mascote


func get_pontos_slot2():
	var slot2 = get_node("Slot2")
	var mascote = slot2.get_child(1)
	
	return mascote.pontos_mascote

func _on_death_remove_slot1():
	remove_slot(1)
	emit_signal("removed")


func _on_death_remove_slot2():
	remove_slot(2)
	emit_signal("removed")
