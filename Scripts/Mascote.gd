extends Node2D

const QTD_MASCOTES = 4
const QTD_ANIMACAO_POR_MASCOTE = 5

var tipo_mascote
var tipo_animacao

var mascote
var nome_animacao

var animacao

var pontos_mascote

var animated_sprite

signal death


func _ready():
	randomize()
	
	tipo_mascote = int(rand_range(1, QTD_MASCOTES + 1))

	mascote = get_mascote(tipo_mascote)

	animacao = get_node("Sprite");

	animacao.set_animation( gera_nome_animacao(mascote) )

	animacao.play()

	pontos_mascote = get_pontuacao(mascote)
	
	set_process(true)

func init(n):
	QTD_MASCOTES = n
	
	randomize()

	
	tipo_mascote = int(rand_range(1, QTD_MASCOTES + 1))

	mascote = get_mascote(tipo_mascote)

	animacao = get_node("Sprite");

	animacao.set_animation( gera_nome_animacao(mascote) )

	animacao.play()

	pontos_mascote = get_pontuacao(mascote)
	
	set_process(true)


func _process(delta):
	pass


func get_mascote(n):

	if n == 1: return "tux"

	elif n == 2: return "bsd"

	elif n == 3: return "suz"
	
	elif n == 4: return "xfce"
	
	elif n == 5: return "gnu"
	
	elif n == 6: return "kiki"
	
	elif n == 7: return "wilber"
	
	elif n == 8: return "godot"
	
	elif n == 9: return "knq"


func get_pontuacao(mascote):
	
	if mascote == "gnu": return 12
	
	elif mascote == "godot": return 11
	
	elif mascote == "tux": return 10
	
	elif mascote == "knq": return 50
	
	elif mascote == "bsd": return 5
	
	elif mascote == "xfce": return 4
	
	elif mascote == "suz": return 3
	
	elif mascote == "kiki": return 3
	
	elif mascote == "wilber": return 2


func update_mascote(tipo_m, n):
	
	QTD_MASCOTES = n
	
	tipo_mascote = tipo_m

	mascote = get_mascote(tipo_mascote)

	animacao = get_node("Sprite");

	animacao.set_animation( gera_nome_animacao(mascote) )

	animacao.play()

	pontos_mascote = get_pontuacao(mascote)
	


func gera_nome_animacao(m):

	tipo_animacao = int(rand_range(1, QTD_ANIMACAO_POR_MASCOTE + 1))
	nome_animacao = str(m,tipo_animacao)

	return nome_animacao


func set_anim_death():
	get_node("AudioSuccess").play()
	nome_animacao = str(mascote,"-exp")
	animacao.set_frame(0)
	animacao.stop()
	animacao.set_animation(nome_animacao)
	animacao.play()
	animated_sprite = get_node("Sprite")
	animated_sprite.connect("animation_finished", self, "emit_death")
	

func emit_death():
	emit_signal("death")
	queue_free()


func _on_Sprite_animation_finished():
	animacao.set_frame(0)
	animacao.stop()
	animacao.set_animation( gera_nome_animacao(mascote) )
	animacao.play()

func _exit_tree():
	queue_free()
