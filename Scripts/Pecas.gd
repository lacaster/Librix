extends Node

const matrix_TAM_X = 6 
const matrix_TAM_Y = 7

var pecaPre
var level

var velocidade
var qtdTipoPecas


signal add_points(p)

# matrix de slots
var matrix = []

func _ready():
	
	#pecaPre
	level = get_node("../")
	inicializa_matrix()
	pecaPre = load("res://Scenes/Game/Peca.tscn")
	set_process(true)


func inicializa_matrix():
	for x in range(matrix_TAM_X):
		matrix.append([])
		matrix[x] = []
		
		for y in range(matrix_TAM_Y):
			matrix[x].append([])
			matrix[x][y] = null




func gerar_peca(v):

	var newPeca = pecaPre.instance()
	newPeca.velocidade = v
	newPeca.connect("limite", self, "_on_Peca_limite")
	newPeca.connect("removed",self, "move_to_bottom")
	add_child(newPeca)


func _on_Peca_limite(x,y,slot1,slot2):

	x = int(round(x))
	y = int(round(y))
	
	#print("x: ", x)
	#print("y: ", y)
	
	#print("slot1.x: ", slot1.x)
	#print("slot1.y: ", slot1.y)
	
	var r = slot2.r

	matrix[x][y] = slot1
	slot1.set_pos(x,y)
	
	if( r == 1 ):
		matrix[x+1][y] = slot2
		slot2.set_pos(x+1,y)
		
	if( r == 2 ):
		matrix[x][y+1] = slot2
		slot2.set_pos(x,y+1)
		
	if( r == 3 ):
		matrix[x-1][y] = slot2
		slot2.set_pos(x-1,y)
	
	if( r == 4 ):
		matrix[x][y-1] = slot2
		slot2.set_pos(x,y-1)


	#mover_para_baixo(v,r)
	#imprime_matrix()
	encontra_padrao()
	verifica_condicao_derrota()

	gerar_peca(velocidade)
	level.update_preview_slot(qtdTipoPecas)

func encontra_padrao():

	var to_remove = []
	var valid = false

	for y in range(0,7):
		for x in range(1,6):
			var p0 = matrix[x-1][y].mascote.tipo_mascote if matrix[x-1][y] != null else null
			var p1 = matrix[x][y].mascote.tipo_mascote if matrix[x][y] != null else null


			if p0 == p1 and p0 != null and p1 != null:
				#print("[",x-1,",",y,"],[",x,",",y,"]")
				add_to_remove(to_remove, matrix[x-1][y])
				add_to_remove(to_remove, matrix[x][y])
				valid = true


	for x in range(0,6):
		for y in range(1,7):
			var p0 = matrix[x][y-1].mascote.tipo_mascote if matrix[x][y-1] != null else null
			var p1 = matrix[x][y].mascote.tipo_mascote if matrix[x][y] != null else null
			
			if p0 == p1 and p0 != null and p1 != null:
				add_to_remove(to_remove, matrix[x][y-1])
				add_to_remove(to_remove, matrix[x][y])
				valid = true


	for t in to_remove:
		#print("to_remove: [",t.x,",",t.y,"]")
		
		var slot_number = matrix[t.x][t.y].num_pos;
		var peca = matrix[t.x][t.y].get_node("../")
		matrix[t.x][t.y] = null
		
		
		if(slot_number == 1):
			var pontos = int( peca.get_pontos_slot1() )
			emit_signal( "add_points", pontos )
			#peca.remove_slot1()
			peca.get_node("Slot1").mascote.set_anim_death()

		elif(slot_number == 2):
			var pontos = int( peca.get_pontos_slot2() )
			emit_signal("add_points", pontos )
			#peca.remove_slot2()
			peca.get_node("Slot2").mascote.set_anim_death()
		
		if(!peca.has_node("Slot1") and !peca.has_node("Slot2")):
			peca.queue_free()




func move_to_bottom():


	# run line
	# run column
	var global_move = false
	
	for y in range(6,-1,-1):
		
		for x in range(0,6):
		
			if matrix[x][y] != null:
				
				var moved = false
				var to_y
				
				for i in range(y, 6):
					if matrix[x][i+1] == null:
						to_y = i+1
						moved = true
					else:
						break
				
				if moved:
					
					#print("[",x,",",y,"] >> [",x,",",to_y,"]")

					matrix[x][y].move_vertical_to(to_y)
					matrix[x][to_y] = matrix[x][y]
					matrix[x][to_y].set_pos(x,to_y)
				    #if matrix[x][to_y] is slot2 move one more time
					matrix[x][y] = null
					
					global_move = true

	if(global_move):
		encontra_padrao()

func verifica_condicao_derrota():
	# existe peca na linha 7?
	
	var preenchido = false
	
	for x in range(0,6):
		if(matrix[x][0] != null):
			preenchido = true

	if(preenchido):
		get_tree().paused = true
		Transition.stop_music()
		get_node("../AudioLose").play()

		for y in range(matrix_TAM_Y):
			for x in range(matrix_TAM_X):
				matrix[x][y] = null

		preenchido = false
		Transition.put_above("res://Scenes/Game/GameOver.tscn")



func imprime_matrix():
	var linha = [0,0,0,0,0,0];

	for y in range(matrix_TAM_Y):
		for x in range(matrix_TAM_X):
			if(matrix[x][y] != null):
				linha[x] = matrix[x][y].mascote.tipo_mascote
			else:
				linha[x] = 0
				
		print(linha[0], " ", linha[1], " ", linha[2], " ", linha[3], " ", linha[4], " ", linha[5])

	print("------------")


func add_to_remove(list, obj):
	if not list.has(obj):
		list.append(obj)

func _exit_tree():

	for c in get_children():
		c.free()
	
	queue_free()
