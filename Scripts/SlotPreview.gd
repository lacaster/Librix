extends Node2D

var mascote_preload
var mascote
var num_pos


func _ready():
	mascote_preload = load("res://Scenes/Game/Mascote.tscn")

func gera_mascote_1(n):
	mascote = mascote_preload.instance();
	mascote.init(n)
	add_child(mascote)
	num_pos = 1

	
func gera_mascote_2(anterior,n):
	mascote = mascote_preload.instance();
	mascote.init(n)
	add_child(mascote)
	num_pos = 2

	while(anterior == mascote.tipo_mascote):
		remove_child(mascote)
		mascote = mascote_preload.instance();
		add_child(mascote)


func remove_mascote():
	remove_child(mascote)

func _exit_tree():

	mascote.free()
	for child in get_children():
		child.free()	
	queue_free()