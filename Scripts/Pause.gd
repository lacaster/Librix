extends Node2D

var bt_yes
var bt_no
var optionChange

func _ready():
	bt_yes = get_node("Control/Yes")
	bt_no = get_node("Control/No")
	bt_no.grab_focus()
	optionChange = get_node("OptionChange")

func _input(event):
	if(Input.is_action_pressed("ui_cancel")):
		Transition.clear_above()
	
	if(Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		optionChange.play()

func _on_No_pressed():
	Transition.clear_above()


func _on_Yes_pressed():
	Transition.free_child_level_node()
	Transition.fade_black_to("res://Scenes/Intro/StageScreen.tscn", null)
	Transition.stop_music()
	Transition.play_music("MusicIntro")
	Transition.clear_above()
	Transition.free_level_node()
	queue_free()
